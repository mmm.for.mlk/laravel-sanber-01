<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $nama_depan = $request->nama;
        $nama_belakang = $request->belakang;

        return view('welcome', [
            'depan' => $nama_depan,
            'belakang' => $nama_belakang,
        ]);
    }
}
