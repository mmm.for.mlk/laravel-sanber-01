<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3> <br>
    
    <form action="/register/kirim" method="post">
      @csrf
        <label for="name">First name :</label> <br> <br>
        <input type="text" id="name" name="nama" required> <br> <br>

        <label for="last">Last name :</label> <br> <br>
        <input type="text" id="last" name="belakang" required> <br> <br>

        <label for="Gender">Gender</label> <br> <br>
        <input type="radio" name="Gender" id="Gender" value="1" required> Male <br>
        <input type="radio" name="Gender" id="Gender" value="2" required> Female <br>
        <input type="radio" name="Gender" id="Gender" value="3" required> Other <br> <br>

        <label for="national">Nationality</label> <br> <br>
        <select name="nationality" id="national" required>
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Thailand</option>
        </select> <br> <br>

        <label for="language">Language Spoken</label> <br> <br>
        <input type="checkbox" name="bahasa" id="language">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" id="language">English <br>
        <input type="checkbox" name="bahasa" id="language">Other <br> <br>

        <label for="biodata">Bio</label> <br> <br>
        <textarea name="bio" id="biodata" cols="30" rows="10" required></textarea> <br>

        <button type="submit">Sign Up</button>

    </form>
</body>
</html>